<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/





Auth::routes();

Route::get('/', 'HomeController@index');


/* Busqueda */

Route::get('busqueda','ProductoController@buscar')->name('buscarProducto');

/* Home */

Route::get('home','HomeController@index')->name('home');

/* Producto */

Route::get('productos','ProductoController@index')->name('productos');
Route::get('producto/{id}','ProductoController@show')->name('producto');
Route::get('agregarProducto','ProductoController@formularioProducto')->name('formularioProducto');
Route::post('producto/guardar','ProductoController@store')->name('registrarProducto');
Route::get('producto/modificar/{producto}','ProductoController@modificar')->name('modificarProducto');
Route::put('producto/{producto}','ProductoController@update')->name('registrarUpdate');
Route::delete('abm/{producto}','ProductoController@destroy')->name('eliminarProducto');

/* FAQ */

Route::get('faq','FaqController@index')->name('faq');
Route::get('abmListaFAQ','FaqController@abm')->name('abmListaFAQ')->middleware('auth');
Route::get('listaFAQ','FaqController@lista')->name('listaFAQ')->middleware('auth');
Route::post('guardarFAQ','FaqController@store')->name('guardarFAQ')->middleware('auth');
Route::get('modificarFAQ/{faq}','FaqController@edit')->name('modificarFAQ')->middleware('auth');
Route::put('modificarFAQ/{faq}','FaqController@update')->name('modificarFAQ')->middleware('auth');
Route::delete('eliminarFAQ/{faq}','FaqController@destroy')->name('eliminarFAQ')->middleware('auth');


/* Categorias */

Route::get('abmListaCategorias','CategoriaController@abm')->name('abmListaCategorias')->middleware('auth');
Route::get('altaCategoria','CategoriaController@create')->name('altaCategoria')->middleware('auth');
Route::post('registrarCategoria','CategoriaController@store')->name('registrarCategoria')->middleware('auth');
Route::get('modificarCategoria/{id}','CategoriaController@edit')->name('modificarCategoria')->middleware('auth');
Route::get('vistaPorCategoria/{id}','CategoriaController@show')->name('productosPorCategoria');
Route::put('modificarCategoria/{id}','CategoriaController@update')->name('modificarCategoria')->middleware('auth');
Route::delete('eliminarCategoria/{item}','CategoriaController@destroy')->name('eliminarCategoria')->middleware('auth');

/* Contacto */

Route::get('contacto','ContactoController@index')->name('contacto');
Route::post('contacto','ContactoController@store')->name('guardarConsulta');
Route::get('consultas','ContactoController@show')->name('consultas');

/* Usuario */

Route::get('perfil','UsuarioController@show')->name('perfil')->middleware('auth');
Route::get('perfil/modificiar/','UsuarioController@edit')->name('modificarPerfil')->middleware('auth');
Route::put('perfil/{user}/modificar/', 'UsuarioController@update' )->name('registrarEdicion')->middleware('auth');
Route::get('perfil/compras','UsuarioController@compras')->name('comprasUsuario')->middleware('auth');
Route::get('perfil/login','UsuarioController@login')->name('loginform');


/* Admin */

Route::get('panel','UsuarioController@paneladministrador')->name('panel')->middleware('auth');
Route::get('abm','ProductoController@abm')->name('abmListaProductos');
Route::get('abm/busqueda','ProductoController@busquedaAbm')->name('abmBusqueda');
Route::get('ventas','ComprasController@index')->name('ventasRealizadas');
Route::get('panel/compra/{venta}','ComprasController@venta')->name('venta');
Route::get('panel/cliente/{cliente}','UsuarioController@perfilcliente')->name('perfilcliente');
Route::post('panel/ventas/buscar','ComprasController@search')->name('buscarventa');

/* Carrito */
Route::get('carrito/direccionenvio','CarritoController@seleccionardomicilio')->name('domiciliocompra')->middleware('auth');
Route::post('carrito/controlpedido','CarritoController@controlpedido')->name('controlpedido')->middleware('auth');
Route::post('carrito/agregar/{producto}','CarritoController@agregar')->name('agregarAlCarrito')->middleware('auth');
Route::get('carrito/remover/{item}','CarritoController@remover')->name('removerItem')->middleware('auth');
Route::get('carrito/registro/{domicilio}','CarritoController@registrarPedido')->name('registrarPedido')->middleware('auth');
Route::get('carrito','CarritoController@show')->name('carrito')->middleware('auth');



/* Compras */

Route::get('mostrarCompras','ComprasController@index')->name('compras')->middleware('auth');
Route::get('verCompra/{compra}','ComprasController@show')->name('compra')->middleware('auth');


/* Domicilio envío */

Route::get('perfil/direccion/lista','DomicilioController@index')->name('direcciones')->middleware('auth');
Route::get('perfil/direccion/nueva','DomicilioController@create')->name('creardomicilio')->middleware('auth');
Route::get('perfil/direccion/editar'.'DomicilioController@edit')->name('editardomicilio')->middleware('auth');
Route::put('perfil/direccion/actualizar','DomicilioController@update')->name('actualizardomicilio')->middleware('auth');
Route::post('perfil/direccion/guardar','DomicilioController@store')->name('guardardomicilio')->middleware('auth');

