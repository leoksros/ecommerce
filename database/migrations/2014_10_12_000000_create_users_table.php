<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('dni')->nullable();
            $table->string('email')->unique();
            $table->string('apellido', 100)->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->string('password');            
            $table->string('codigoarea')->nullable();
            $table->string('telefono')->nullable();
            $table->integer('provincia')->nullable();
            $table->string('id_avatar')->nullable();
            $table->boolean('administrador');
            $table->rememberToken();
            $table->timestamps();


        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
