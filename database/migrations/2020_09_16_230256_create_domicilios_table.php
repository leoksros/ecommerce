<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDomiciliosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('domicilios', function (Blueprint $table) {
            
            $table->engine = 'InnoDB';
            $table->bigIncrements('id')->unsigned();
            $table->timestamps();
            $table->string('direccion',255)->nullable();
            $table->string('altura',10)->nullable();
            $table->string('piso',5)->nullable();
            $table->string('departamento',5)->nullable();
            $table->string('ciudad',255)->nullable();
            $table->string('provincia',255)->nullable();
            $table->string('cp',10)->nullable();
            $table->string('comentario',255)->nullable();
            $table->bigInteger('id_user')->unsigned();            

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('domicilios');
    }
}
