<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        
        
        DB::table('categorias')->insert(
            [
                'nombre' => 'General',
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')
            ]
        );

        DB::table('avatars')->insert(
            [
                'nombre' => './img/faq.png',
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')

            ]
        );

        

        /* DB::table('provincias')->insert(
            [
                'nombre' => "Santa Fe"
            ]
        );

        DB::table('ciudads')->insert(
            [
                'nombre' => "Rosario",
                'id_provincia' => '1',
                'cp' => '2000',
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')
            ]
        );

        DB::table('ciudads')->insert(
            [
                'nombre' => "San Lorenzo",
                'id_provincia' => '2',
                'cp' => '2200',
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')
            ]
        );

        DB::table('ciudads')->insert(
            [
                'nombre' => "Clason",
                'id_provincia' => '3',
                'cp' => '2144',
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')
            ]
        ); */


        DB::table('users')->insert(
            [
                'name'	=> "Leonardo",
                'apellido'	=> "Miceli",                
                'email'	=> "leo@nardo.com",                
                'password'	=> Hash::make('probando'),
                'codigoarea'	=> "0341",
                'telefono'	=> "3340836",
                'id_avatar'     => "NULL",
                'administrador'	=> "1",
                'created_at'	=> date('Y-m-d H:m:s'),
                'updated_at'	=> date('Y-m-d H:m:s')
            ]
        );

        
       

    }
}
