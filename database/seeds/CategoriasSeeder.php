<?php

use Illuminate\Database\Seeder;
use Falter\Factory as Faker;

class CategoriasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        factory(App\Categoria::class, 4)->create();

        
    }
}
