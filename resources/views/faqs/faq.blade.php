
@extends('layouts.base')

@section('content')
    <div class="media">
        <img src="./img/faq.png" class="rounded mx-auto d-block img-fluid" alt="Responsive image">
    </div>
    <div>
        <h1 class="text-center">
            Preguntas Frecuentes
        </h1>
    </div>

    
    <div class="container py-3">
        <div class="row">
            <div class="col-12 mx-auto">
                <div class="accordion" id="faqExample">
                    @forelse ($preguntas as $pregunta)

                        <div class="card">
                            <div class="card-header p-2 d-flex justify-content-start" id="{{$pregunta->id}}">
                                <h5 class="mb-0 text-center">
                                    <button class="btn btn-link" type="button" data-toggle="collapse" data-target="#{{$pregunta->id}}" aria-expanded="true" aria-controls="{{$pregunta->id}}">
                                        <i class="fas fa-plus dark-primary "></i>  {{ $pregunta->pregunta }}
                                    </button>
                                </h5>
                            </div>
                            <div id="{{$pregunta->id}}" class="collapse show " aria-labelledby="{{$pregunta->id}}" data-parent="#faqExample">
                                <div class="card-body">
                                    {{ $pregunta->respuesta }}
                                </div>
                            </div>
                        </div>

                    @empty
                    <div class="row text-center d-flex justify-content-center  justify-content-center">

                        <h1 class="tituloArticulo"></h1>
                      </div>
                    @endforelse
                </div>

            </div>
        </div>
    </div>



@endsection
