@extends('admin.paneladministrativo')

@section('content')


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

            @if (session('status'))
                <div class="alert alert-success text-center">
                    {{ session('status') }}
                </div>
            @endif

            <div class="card">
               
                <div class="card-header text-center">{{ __('Lista de FAQs') }}   </div>

                    <div class="card-body">
                           
                        
                                       
                        
                        <div class="row pt-3 pb-3">
                            <div class="col-3 border pt-2 pb-2 text-center">Nº</div>
                            <div class="col-3 border pt-2 pb-2 text-center">Pregunta</div>
                            <div class="col-3 border pt-2 pb-2 text-center">Respuesta</div>
                            <div class="col-3 border pt-2 pb-2 text-center ">Acciones</div>
                        </div>
                        <div class="row">

                           

                            @if(count($faqs) > 0)
                                @foreach ($faqs as $faq)

                                
                                   
                                        <div class="col-3">
                                            {{ $faq->id }}
                                        </div>
                                        <div class="col-3">
                                            {{$faq->pregunta}} 
                                        </div>
                                        <div class="col-3">
                                            {{ $faq->respuesta }}                                
                                        </div>
                                        <div class="col-3 text-center">                                        
                                                <a class="btn btn-warning" href="{{route('modificarFAQ',$faq)}}"> <i class="fas fa-pencil-alt"></i></a>   

                                                <form onclick="return confirm('Está seguro de eliminar?')" action="{{route('eliminarFAQ',$faq)}}" method="POST">
                                                    @csrf
                                                    @method('DELETE')
                                                    <button class="btn btn-danger" type="submit"> <i class="fas fa-trash"></i></a>
                                                </form>   
                                        </div>  
                                        
                                
                                        @if(count($faqs) > 1)
                                            <div class="line"></div>
                                        @endif  
                                    
                        
                                    
                                @endforeach
                            @else

                                <div class="col text-center">
                                    <h3>Lista vacía</h3> 
                                </div>  
                            @endif
                
                        </div> 

                    </div>
            </div>        
        </div>
    </div>

                      
        
    <br><br>

          
</div>


@endsection


           


