@extends('admin.paneladministrativo')

@section('content')



<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

           

            <div class="card">
               
                <div class="card-header text-center">{{ __('Pregunta y respuestas') }}   </div>

                    <div class="card-body">
                        
                    <form method="POST" action="{{ route('modificarFAQ',$faq)  }}">
                                                    
                            @csrf
                            @method('PUT')

                            <div class="form-group row">
                                <label for="pregunta" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Pregunta') }}</label>

                                <div class="col-md-6">
                                    <input id="pregunta" type="text" class="form-control @error('pregunta') is-invalid @enderror" name="pregunta" value="{{ $faq->pregunta }}" required autocomplete="pregunta" autofocus>

                                    @error('pregunta')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="respuesta" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Respuesta') }}</label>

                                <div class="col-md-6">
                                    <input id="respuesta" type="text" class="form-control @error('respuesta') is-invalid @enderror" name="respuesta" value="{{ $faq->respuesta }}" required autocomplete="respuesta" autofocus>

                                    @error('respuesta')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            

                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-success">
                                        {{ __('Actualizar') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>

                      
        
    </div>
</div>

@endsection

