@extends('layouts.base')
    

@section('content')


    @if (session('status'))
        <div class="alert alert-success text-center">
            {{ session('status') }}
        </div>
    @endif

    <h3 class="text-center">Perfil</h3>

    <br>

    <div class="row justify-content-center align-items-center">

        {{-- Avatar --}}

        <div class="col-sm-4">
        
            @if (auth()->user()->id_avatar != 'NULL')
                <img src="{{Storage::url(auth()->user()->id_avatar)}}" class="rounded mx-auto d-block img-fluid mb-3" alt="Responsive image" style="max-height: 250px">
            @else           
                <img src="{{asset('img/user.png')}}" class="rounded mx-auto d-block img-fluid mb-3" alt="Responsive image" style="max-height: 250px">
            @endif        
                
        </div>

        <div class="col-12 col-sm-8 text-center">
                            
                            {{-- Apellido  y nombre --}}
                           
                            <div class="col-12 mb-3">

                                @if (auth()->user()->name)
                                    <b class="display-4 ">{{Auth()->user()->name}}</b>                          
                                @endif
                                
                                @if (auth()->user()->apellido)
                                    <b class="display-4 ">{{Auth()->user()->apellido}}</b>                               
                                @endif

                            </div>

                            <div class="col-12">
                                  
                                {{-- Mail usuario --}}    
                                @if (auth()->user()->email)
                                    {{Auth()->user()->email}}                               
                                @endif

                            </div>

                            <div class="col-12">
                                    {{-- Código de área y teléfono --}}
                                    @if (auth()->user()->codigoarea)
                                        {{Auth()->user()->codigoarea}} -                                
                                    @endif
                                    @if (auth()->user()->telefono)
                                        {{Auth()->user()->telefono}}                               
                                    @endif
                            </div>
                           
                            <div class="col-12">
                                {{-- Dirección y altura --}} 
                                    @if (auth()->user()->direccion)

                                        <br>
                                        <b> Dirección de envío</b> 
                                        <br>

                                        {{auth()->user()->direccion}}        

                                    @endif         
                                                    
                                    @if (auth()->user()->altura)

                                        {{auth()->user()->altura}}    

                                    @endif

                                    {{-- Ciudad --}}
                                    @if (auth()->user()->id_ciudad)   

                                        -   @foreach (auth()->user()->provinciaUsuario->ciudad as $ciudad)
                                                
                                                @if (auth()->user()->id_ciudad == $ciudad->id)
                                                    {{ $ciudad->nombre}}
                                                @endif

                                            @endforeach

                                    @endif

                                    <br> 

                                    {{-- Provincia --}}                                 
                                        @if (auth()->user()->provincia)
                                            {{(auth()->user()->provinciaUsuario->nombre)}}                                
                                        @endif
                                    <br> 

                                    {{-- CP  --}}
                                    @if (auth()->user()->cp)
                                            {{auth()->user()->cp}}                                
                                    @endif                                    
                                        
                            </div>

                            <div class="col-12">
                                <a href="{{ Route('modificarPerfil') }}"><button type="button" class="btn btn-success btn-lg mt-5">Modificar</button></a>
                            </div>
                                      
        </div>
          
    </div>    

@endsection

