@extends('layouts.base')


@section('content')
<div class="container ">

    <!-- form  -->
    <form method="POST" action="{{Route('registrarEdicion', Auth::user())}}" class="needs-validation" enctype="multipart/form-data" novalidate>
        @csrf
        @method('PUT')
         <div class="signup-form-container mt-3">
        
            <div class="row tituloArticulo p-3">    

                <div class="form-group col-md-12">
                    <h3 class="text-center">Hola {{auth()->user()->name }} !</h3>
                </div>
   
                <div class="col border rounded bg-white p-3"> 
                        <div class="form-row">   
                        </div>                    
                        <div class="form-row">
                            <div class="form-group col-md-6">
                                    <label for="firstname">Nombre</label>
                                <input type="text" class="form-control" id="firstname" name="name" placeholder="Nombre" value="{{ auth()->user()->name }}" required>  
                            </div>
                            <div class="col">
                                    <label for="lastname">Apellido</label>
                                <input type="text" class="form-control" id="lastname" name="apellido" placeholder="Apellido" value="{{ auth()->user()->apellido }}" required>
                            </div>
                        </div>
                        <div class="form-row">
                                <div class="form-group col-md-6">
                                    <label for="inputEmail4">Email</label>
                                    
                                    <input type="email"  class="form-control" id="inputEmail4" placeholder="Email" name="email" value="{{ auth()->user()->email }}" required>
                                    
                                </div>

                                <div class="form-group col-md-2">
                                    <label for="inputCod">Código área</label> 
                                    <input type="text" class="form-control" id="inputCod" name="codArea" placeholder="Código área" value="{{ auth()->user()->codigoarea }}" required>
                                    
                                </div>
                                <div class="form-group col-md-4">
                                        <label for="inputTel">Teléfono</label> 
                                        <input type="text" class="form-control" id="inputTel" name="telefono" placeholder="Teléfono" value="{{ auth()->user()->telefono }}" required>
                                </div>
                  
                        </div>

                 <button type="submit" class="btn btn-success" >Aceptar</button>
                    
                </div>

                </div>
         </div>
         </form>
      </div>
      


@endsection
    
 
