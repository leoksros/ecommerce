@extends('layouts.base')

@section('content')
    

<div class="container-fluid">


  
 


    <div class="row">




      <div class="col-sm-12 col-lg-6 mb-3">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
          
          <div class="carousel-inner">
            <div class="carousel-item active">
              <img src="img/bollos.jpg" class="d-block w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="img/hamburguesas.jpg" class="d-block  w-100" alt="...">
            </div>
            <div class="carousel-item">
              <img src="img/pizza.jpg" class="d-block   w-100" alt="...">
            </div>
          </div>

          <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>

          <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>

        </div>
      </div>

      <div class="col-sm-12 col-lg-6" style="background-color: wheat;">
        <div class="jumbotron jumbotron-fluid" style="background-color: wheat;">
            <div class="container-fluid">
                <h1 class="display-4">About us!</h1>
                <p class="lead">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Ex vel soluta, qui quasi adipisci voluptatum! Sunt perferendis cupiditate exercitationem placeat autem accusantium atque deserunt, eveniet, aperiam omnis consequatur repudiandae nam!</p>
            </div>
        </div>
      </div>

    </div>

    <div class="row d-flex justify-content-center">
    
      @if (count($productos) != 0)
          <div class="col-lg-12 mt-5 mb-5 d-flex justify-content-center text-center">
              <h1>
                <hr>
                  Productos destacados!
                <hr>
              </h1>
          </div>
      @endif     

      @forelse ($productos as $producto)

        <div class="col col-sm-6 mb-3 col-md-3 col-lg-2">
          <div class="card h-100 ">
                                            
              @foreach ($producto->imagenes as $imagen)
                <div class="h-100 text-center">
                  <img src="{{asset("/storage/$imagen->nombre")}}" class="mx-auto img-fluid  align-middle" alt="...">
                </div>                        
              @endforeach

              <div class="card-body">
                <a href=" {{url('producto')."/". $producto->id }}" class="tituloArticulo">
                  <h5 class="card-title">{{$producto->nombre}}</h5>
                </a>
              </div>

              <div class="card-footer">
                <small class="text-muted font-weight-bold">$ {{$producto->precio }}</small>
              </div>

          </div>
        </div>  

      @empty
          
      @endforelse

    </div>

</div>

@endsection  
