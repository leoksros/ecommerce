@extends('admin.paneladministrativo')


@section('content')

<div class="container">

    <div class="row d-flex justify-content-center">
    
    
        <div class="col">
            
            @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
            @elseif(session('no'))
                    <div class="alert alert-danger text-center">
                        {{ session('no') }}
                    </div>
            @endif



            

            <form method="GET" action="{{ route('abmBusqueda') }}">
                @csrf
                <div class="input-group mb-3">
                  <input type="text" class="form-control" name="busqueda" placeholder="Ingresar búsqueda" aria-label="Recipient's username" aria-describedby="basic-addon2">
                  <div class="input-group-append">
                    <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
                  </div>
                </div>
            </form>
    
            <ul class="list-group">
    
               
                
                @forelse ($productos as $producto)
                <br>
                     <li class="list-group-item">
                         <div class="d-flex row justify-content-around">
    
    
                            <div class="col">
                                @foreach ($producto->imagenes as $imagen)
                                    <img src="{{asset("/storage/$imagen->nombre")}}"  alt="" style="max-width: 150px" class="img-fluid align-middle ">
                                @endforeach         
                             </div>
    
    
                             <div class="col">
                                 ID: {{$producto->id}}
                               <a href="{{url('producto')."/$producto->id"}}">{{ $producto->nombre }}</a> 
                             </div>
    
                             <div class="col">
                                 <a href=" {{route('modificarProducto', $producto)}}">Modificar</a>
    
                                <form action="{{route('eliminarProducto', $producto)}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <button class="btn btn-link" type="submit">Eliminar</button>                                    
                                </form>
    
                                 
                            </div>
                         </div>                    
                     </li>
                @empty
                <div class="row d-flex justify-content-center  justify-content-center">
    
                    <h1 class="tituloArticulo"> No se encuentran registros :'(</h1>
                  </div>
                @endforelse
                
            </ul>
        </div>
    </div>
       
</div>

@endsection