@extends('layouts.base')

@section('content')


    <div  class="container-fluid fondoRosaClaro">

      <div class="row p-5 justify-content-center align-items-center">

            <div class="col-12 col-sm-6 col-md-4">
                  @foreach ($producto->imagenes as $imagen)
                    <img  src="{{asset("/storage/$imagen->nombre")}}"  alt="" class="mx-auto img-fluid align-middle">
                  @endforeach
            </div>          
          
          <div class="col-12 col-sm-6 col-lg-3">

              <div class="row fondoRosa">
                      <div class="col mt-3 mb-3 font-weight-bold">
                      <p class="h5 tituloArticulo">{{$producto->categoria->nombre}}</p>
                      <p class="h3 tituloArticulo">{{$producto->nombre}}</p>
                      </div>
              </div>

              <form method="POST" action="{{ route('agregarAlCarrito', $producto)  }}">

                  @csrf
                  <div class="row">

                      <div class="col">
                              <br>    

                              <p>{{$producto->descripcion}}</p>                            
                              <b>Valor: </b>{{$producto->precio}}
                              <br>
                              
                            
                              

                              <div class="row justify-content-between">
                              
                                <div class="col-2">
                                  <p class="font-weight-bold">Cantidad:  </p>                                                                                                
                                </div>

                                <div class="col-9">
                                  <input type="text" class="form-control" min="1" name="cantidad" required>
  
                                    @error('cantidad')
                                      <span class="invalid-feedback" role="alert">
                                          <strong>{{ $message }}</strong>
                                      </span>
                                    @enderror   
                                </div>
                              </div>
                              <br>
                      </div>                        
                  </div> 
                  
                  <div class="d-flex justify-content-center mt-3">                    
                      <button type="submit" class="btn btn-success btn-lg">Agregar al carrito</button>
                  </div>
                  <p></p>

              </form>   

          </div>   
      </div>    
      
      <div class="row p-5 border fondoNaranjaClaro">
          <h2 class="">También podría interesarte..</h2>
        
          <div class="container-fluid row d-flex text-center">

            @foreach ($productosCategoria as $productoAbajo)
                @if ($producto->id != $productoAbajo->id )
                  <div class="col-sm-6 col-md-6 col-lg-2 mb-3 mt-3">                                          
                      <div class="card h-100">

                       
                             
                          
                            
                            <div class="card-body">      
                              @foreach ($productoAbajo->imagenes as $imagen)
                              <img src="{{asset("/storage/$imagen->nombre")}}" class="mx-auto img-fluid " alt="...">
                        @endforeach
                            </div>                        
                                  <a href="{{ url('producto')."/$productoAbajo->id" }}">
                                  <h5 class="card-title">{{$productoAbajo->nombre}}</h5></a>                        
                            

                            <div class="card-footer">
                              <small class="text-muted font-weight-bold">{{$productoAbajo->precio}}</small>
                            </div>
                            
                      </div>                                                                            
                  </div>    
                @endif                  
              @endforeach     
                
          </div>
      </div>
          
    </div>

@endsection

    
