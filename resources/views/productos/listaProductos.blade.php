@extends('layouts.base')

@section('content')


  <div class="container-fluid pt-3 fondoRosaClaro">

    <div class="row justify-content-center">

      @if (count($categorias) > 1)                  
        <div class="col-sm-11 col-md-2  mb-3 fondoRosa rounded">

          <div class="row color-marron p-1 text-light h6 pb-3 pt-3 pl-2 rounded-top">
            Categorías
          </div>          

          <ul class="nav flex-md-column justify-content-between">

            @foreach ($categorias as $categoria)
                <li class="nav-item">
                  <a class="nav-link active  tituloArticulo" href="{{route('productosPorCategoria',$categoria->id)}}"><i class="fas fa-plus dark-primary "></i>{{$categoria->nombre}}</a>
                </li>                
            @endforeach  

          </ul>  

        </div>

      @else         
     
      @endif    
      

      <div class="col-sm-12 col-md-9">
        <div class="row">

          @forelse ($productos as $producto)
            <div class="col-12 col-sm-6 col-md-6 col-lg-3 mb-3">

              <div class="card text-center h-100">

                @foreach ($producto->imagenes as $imagen)
                    <div class="h-100">
                      <img src="{{asset("/storage/$imagen->nombre")}}" class="mx-auto img-fluid align-middle">
                    </div>                    
                @endforeach
                
                <div class="card-body">
                    <a href=" {{url('producto')."/$producto->id"}}" class="tituloArticulo">
                      <h5 class="card-title">{{$producto->nombre}}</h5>
                    </a>
                </div>

                <div class="card-footer">
                  <small class="text-muted font-weight-bold">${{$producto->precio}}</small>
                </div>

              </div>
              
            </div>
          @empty

          <div class="col">
              <div class="row d-flex justify-content-center  justify-content-center">
                <h1 class="tituloArticulo"> No se encontraron resultados. </h1>
              </div>
          </div>
          @endforelse
                
        </div>

      </div>

    </div>

  </div>
@endsection

  



