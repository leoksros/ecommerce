@extends('admin.paneladministrativo')

@section('content')

@php
    
@endphp
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">{{ __('Actualizar artículo') }}</div>

                <div class="card-body">
                    <form method="POST" action="{{ route('registrarUpdate', $producto) }}" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')

                        <div class="row">

                            <div class="col">
                                <div class="form-group row">
                                    <label for="nombre" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>
        
                                    <div class="col-md-6">
                                        <input  id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{$producto->nombre}}" required autocomplete="nombre" autofocus>
        
                                        @error('nombre')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="precio" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Precio') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="precio" type="text" class="form-control @error('precio') is-invalid @enderror" name="precio" value="{{$producto->precio}}" required autocomplete="precio" autofocus>
        
                                        @error('precio')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="stock" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Stock') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="stock" type="number" class="form-control @error('stock') is-invalid @enderror" name="stock" value="{{$producto->stock}}" required autocomplete="stock">
        
                                        @error('stock')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="categoria_id" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Categoria') }}</label>
        
                                    <div class="col-md-6">
                                        {{-- <input id="categoria_id" type="text" class="form-control @error('categoria_id') is-invalid @enderror" name="categoria_id" value="{{$producto->categoria_id}}" required autocomplete="categoria_id" autofocus> --}}
                                        <select class="form-control" name="categoria_id" id="categoria_id">

                                            @foreach ($categorias as $categoria)
                                                <option value="{{ $categoria->id }}">{{$categoria->nombre}}</option>                                        
                                            @endforeach
        
                                        </select>
                                        @error('categoria_id')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>
        
                                <div class="form-group row">
                                    <label for="descripcion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Descripcion') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="descripcion" type="text" class="form-control @error('descripcion') is-invalid @enderror" name="descripcion" value="{{$producto->descripcion}}" required autocomplete="descripcion" autofocus>
        
                                        @error('descripcion')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>                    
        
                                
                                
        
                                
                            </div>

                            <div class="col-sm-12 col-lg-5">
                                @foreach ($producto->imagenes as $imagen)
                                    <img src="{{asset("/storage/$imagen->nombre")}}"  alt="" class="img-fluid align-middle">
                                @endforeach  
                                <div class="form-group row">
                                    <label for="productoimagen" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Cambiar imágen') }}</label>
        
                                    <div class="col-md-6">
                                        <input id="productoimagen" type="file" class="form-control @error('productoimagen') is-invalid @enderror" name="productoimagen" value="{{ old('productoimagen') }}" autocomplete="productoimagen" autofocus>
        
                                        @error('productoimagen')
                                            <span class="invalid-feedback" role="alert">
                                                <strong>{{ $message }}</strong>
                                            </span>
                                        @enderror
                                    </div>
                                </div>                          
                            </div>

                            

                        </div>

                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Guardar cambios') }}
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>


@endsection