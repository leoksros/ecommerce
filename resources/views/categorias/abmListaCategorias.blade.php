@extends('admin.paneladministrativo')

@section('content')
    
  
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">

    @if (session('status'))
                <div class="alert alert-success text-center">
                    {{ session('status') }}
                </div>
        @elseif(session('no'))
                <div class="alert alert-danger text-center">
                    {{ session('no') }}
                </div>
        @endif

        <div class="card">
               
            <div class="card-header text-center">{{ __('Categorías') }}   </div>

                <div class="card-body">


            


        

            @forelse ($categorias as $item)
            
            <li class="list-group-item">
                <div class="d-flex row text-center justify-content-around">
                    <div class="col tituloArticulo">    
                        {{$item->nombre}}
                        <br>
                    </div>

                    <div class="col">
                        <a href="{{route('modificarCategoria',$item->id)}} ">Modificar</a>
                    <form action="{{route('eliminarCategoria',$item)}}" method="POST">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-link" type="submit">Eliminar</button>                                    
                    </form>                            
                </div>
                </div>   
            </li>
        @empty

            <div class="row d-flex justify-content-center  justify-content-center">

                <h1 class="tituloArticulo"> No se encuentraron registros.</h1>
            </div>    

        @endforelse

                </div>
        </div>
        </div>


        </div>
    </div>
</div>
@endsection