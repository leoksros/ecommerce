@extends('admin.paneladministrativo')

@section('content')
    

<div class="card">
               
    <div class="card-header text-center">{{ __('Nueva categoría') }}   </div>

        <div class="card-body">
            <form method="POST" action="{{route('registrarCategoria')}}" enctype="multipart/form-data">
                        @csrf   
                        <div class="form-group row">
                            <label for="nombre" class="col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

                            <div class="col-md-6">
                                <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="name" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                                @error('nombre')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>  
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-primary">
                                    {{ __('Registrar') }}
                                </button>
                            </div>
                        </div>          
                    </form>
      
        </div>          </div>           
</div>

@endsection