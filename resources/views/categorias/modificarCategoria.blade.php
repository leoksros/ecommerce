@extends('admin.paneladministrativo')

@php
    /* dd($id,$categoria); */
@endphp

@section('content')
    
       
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center">{{ __('Actualizar categoría') }}</div>

                <div class="card-body">

<form method="POST" action="{{route('modificarCategoria',$id)}}" enctype="multipart/form-data">
    @csrf   
    @method('PUT')

    <div class="form-group row">
        <label for="nombre" class="text-center tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Nombre') }}</label>

        <div class="col-md-6">
            <input id="nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" placeholder="{{ $categoria->nombre }}" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

            @error('nombre')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
    </div>  
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-success">
                {{ __('Actualizar') }}
            </button>
        </div>
    </div>        
    
    
</form>


                </div>
            </div>

            </div>
        </div>
    </div>
</div>



@endsection