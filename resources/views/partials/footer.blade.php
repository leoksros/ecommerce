@extends('layouts.base')
 
 
@section('footer')
    
    <!-- Ini. Footer -->
    <footer>
        <div class="sticky-container">
            <ul class="sticky">
                <li>
                    <img src="img/carrito.png" width="32" height="32">
                    <p><a href="./vistaCarrito.php" target="_blank">Carrito de<br>Compras</a></p>
                </li>
                <li>
                    <img src="img/facebook-circle.png" width="32" height="32">
                    <p><a href="https://web.facebook.com" target="_blank">Seguinos en<br>Facebook</a></p>
                </li>
                <li>
                    <img src="img/instagram-circle.png" width="32" height="32">
                    <p><a href="https://www.instagram.com" target="_blank">Seguinos en<br>Instagram</a></p>
                </li>
                <li>
                    <img src="img/whatsapp-circle.png" width="32" height="32">
                    <p> <a href="whatsapp://send?text=Titulo-Enlace" data-action="share/whatsapp/share" target="_blank">Contactanos por<br>whatsapp</a></p>
                </li>
            </ul>
        </div>
    </footer>
            <!-- Fin Footer -->

@endsection
