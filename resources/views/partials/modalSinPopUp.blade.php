<div class="container-fluid">
            <div class="modal-content text-center modal-dialog" style="margin-top: 10%">
                        <div class="col-12 user-img">
                                <img src="img/user.png" th:src="@{/img/user.png}"/>
                        </div>
                        
                        <form class="col-12"  method="POST">
                                <div class="form-group formlogingroup" id="user-group">
                                    <input type="text" class="form-control <?=$emailValidacion?>" placeholder="Email" value="<?=$email?>" name="email"/>
                                    <?=$errorEmail?>
                                </div>

                                <div class="form-group formlogingroup" id="contrasena-group">
                                    <input type="password" class="form-control <?=$passValidacion?>" value="<?=$password?>" placeholder="Contrasena" name="password"/>
                                    <?=$errorPassword?>
                                </div>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fas fa-sign-in-alt"></i>Ingresar
                                </button>                           
                                <p></p>
                                <p></p>                                
                                <a href="#" class="text-white">Recordar contraseña?</a>                                
                                        <a  class="text-white" href="./registro.php">
                                            | Registrarse     
                                        </a>       
                                        <p></p>                     
                        </form>                 
            </div>
</div>
