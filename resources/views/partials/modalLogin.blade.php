@extends('layouts.base')

@section('modal')
    
    <?php
    /* require_once 'controladores/login.php'*/
    ?>

    <!-- Modal -->
    <div class="modal fade text-center" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="col-12 user-img">
                    <img src="img/user.png" th:src="@{/img/user.png}" />
                </div>
                <form class="col-12" method="POST">
                    <div class="form-group formlogingroup" id="user-group">
                        <input type="text" class="form-control <?= $emailValidacion ?>" placeholder="Email" value="<?= $email ?>" name="email" />
                        <?= $errorEmail ?>
                    </div>

                    <div class="form-group formlogingroup" id="contrasena-group">
                        <input type="password" class="form-control <?= $passValidacion ?>" value="<?= $password ?>" placeholder="Contrasena" name="password" />
                        <?= $errorPassword ?>
                    </div>
                    <div class="row">
                        <div class="col">
                            <input type="checkbox" name="recordarme" value="recordarme"> Recordarme
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">
                        <i class="fas fa-sign-in-alt"></i>Ingresar
                    </button>
                    <p></p>
                    <p></p>
                    <a href="./recuperarpassword.php" class="text-white">Olvidaste tu contraseña?</a>
                    <a class="text-white" href="./registro.php">
                        | Registrarse
                    </a>
                    <p></p>
                </form>
            </div>
        </div>

    </div>
    <!-- Fin Modal -->

@endsection
