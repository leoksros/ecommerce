 <?php
 
    require_once 'controladores/login.php';
    ?>
 <!--NavBar-->
 <nav class="navbar navbar-expand-lg navbar-light color-marron mb-4 pr-4">

     <div class="container-fluid d-flex justify-content-around">
         <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
             <span class="navbar-toggler-icon  "></span>
         </button>
         <div class="col d-flex">
             <a class="navbar-brand mx-auto d-block" href="./home.php"><img src="./img/logo/logoNuevoBlanco.png" alt="" class="img-fluid"></a>
         </div>
         <!-- Button trigger modal -->
         <!-- <button type="button" class="btn btn-primary              navbar-toggler    " data-toggle="modal" data-target="#exampleModalCenter">
        <i class="fas fa-user-circle fa-2x text-light"  ></i>  
    </button> -->

         <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
             <ul class="navbar-nav mr-auto">
                 <li class="nav-item active">
                     <a class="nav-link text-white" href=".\home.php">Home <span class="sr-only">(current)</span></a>
                 </li>
                 <!-- <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            Productos
        </a>
        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
            <a class="dropdown-item" href="#">Articulos de un tipo 1</a>
            <a class="dropdown-item" href="#">Articulos de un tipo 2</a>
            <a class="dropdown-item" href="#">Articulos de un tipo 2</a>
        </div>-->
                 </li>
                 <li class="nav-item">
                     <a class="nav-link text-white " href=".\listaProductos.php">Productos</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link text-white" href=".\faq.php">F.A.Q</a>
                 </li>
                 <li class="nav-item">
                     <a class="nav-link text-white" href=".\contacto.php">Contacto</a>
                 </li>

             </ul>
             <form class="form-inline my-2 my-lg-0 ">
                 <input class="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search">
                 <button class="btn btn-success my-2 my-sm-0" type="submit">
                     <i class="fas fa-search"></i>
                 </button>
             </form>
         </div>

            
         <?php if(isset($_SESSION['email'])){
         $variable = $cuentas[buscaSession()]["avatar"];}
         ?>
         

         <div class="ml-3 border border-light">
             <?= isset($_SESSION['email']) ? '<a href="controladores/logout.php" class="ml-3 estiloLink1"> Logout </a>' : '<a href="" class="ml-3 estiloLink1"  data-toggle="modal" data-target="#exampleModalCenter"> Login | Sign in </a>' ?>
             <button type="button" class="btn" <?= isset($_SESSION['email']) ? '' : 'data-toggle="modal" data-target="#exampleModalCenter"'?>>
             <?= isset($_SESSION['email']) ? '<a href="./perfil.php"><img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 50px" src="archivos/'.$variable.'"> </a>' : '<i class="fas fa-user-circle fa-2x text-light"></i>'?>
             </button>
         </div>
     </div>

 </nav>
 <!--Fin NavBar-->

