@extends('layouts.base')
    

@section('content')


    <div class="container-fluid">
        @if (session('status'))
    <div class="alert alert-success text-center">
        {{ session('status') }}
    </div>
    @endif

    <h3 class="">Mis compras</h3>
    <br>
        <div class="card">
            
            <div>
                
                    @forelse (auth()->user()->compras as $compra )
                        
                        <ul class="list-group list-group-flush">                            
                            <li class="list-group-item mb-2">

                                    <div class="row justify-content-between mb-5">
                                        <div class="col-6 lead font-weight-bold">    
                                            Compra #{{$compra->id}}                                            
                                        </div>
                                        
                                   
                                        <div class="col-6 text-right">                                        
                                            <a href="{{route('compra',$compra)}}" class="text-primary">Ver compra</a>
                                        </div>
                                    </div>
                                    
                                    <div class="row d-flex justify-content-around ">


                                        <div class="col-12">
                                            
                                            @foreach ($compra->items as $item)   
                                            
                                                <ul class="list-group list-group-flush">
                                                    <li class="" style="list-style:none">
                                                        <div class="row p-2">

                                                            @foreach ($item->producto->imagenes as $imagen)
                                                                <div class="col-3"><img style="" class="img-fluid" src="{{asset("/storage/$imagen->nombre")}}" alt=""> </div>                                                        
                                                            @endforeach     
                                                            
                                                            <div class="col-8">
                                                                <a href="">{{$item->producto->nombre}}</a>
                                                                <br>
                                                                {{$item->producto->descripcion}}
                                                                <br>
                                                                ${{$item->precio}} x 
                                                                {{$item->cantidad}} unidad/es

                                                            </div>                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             
                                                        </div>
                                                        <hr>
                                                    </li>                                                    
                                                </ul>                                            
                                            @endforeach

                                        </div>

                                        <div class="col-12 text-right mb-4">                                                
                                                <br>
                                               <b>Total compra:</b>  {{ $compra->total() }}
                                        </div>                                                                         
                                    </div>       
                                                         
                            </li>                            
                        </ul>

                    @empty

                        <div class="card-body text-center">

                           <h3>No tenemos ninguna compra registrada :)</h3> 

                        </div>

                    @endforelse                   
                
            </div>

        </div>

    </div>    

@endsection

