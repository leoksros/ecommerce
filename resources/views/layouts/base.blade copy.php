<!DOCTYPE html>

    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Los iconos tipo Solid de Fontawesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
                
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('./css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('./css/index.css')}}" th:href="@{/css/index.css}">
        {{-- <link rel="stylesheet" type="text/css" href="css/style.css"> --}}
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" defer>  </script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous" defer>  </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" defer >  </script>
        <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js" defer></script>
            
        <title>Integralísimo</title>

    </head>
    
    <body class="fondoRosaClaro">
        
        
        <header>
            <nav class="navbar navbar-expand-lg navbar-light color-marron mb-4 pr-4">
    
                <div class="container-fluid d-flex justify-content-around">
                    <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon  "></span>
                    </button>
                    <div class="col d-flex">
                        <a class="navbar-brand mx-auto d-block" href=" {{ route('home') }} "><img src="{{asset('./img/logo/logoNuevoBlanco.png')}}" alt="" class="img-fluid"></a>
                    </div>
                    
                    <!-- Button trigger modal -->    
        
                    <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
                        <ul class="navbar-nav mr-auto">
                            <li class="nav-item active">
                                <a class="nav-link text-white" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                            </li>                
                            

                            <li class="nav-item">
                            <a class="nav-link text-white " href=" {{ route('productos') }}">Productos</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-white" href=" {{ route('faq') }} ">F.A.Q</a>
                            </li>

                            <li class="nav-item">
                                <a class="nav-link text-white" href=" {{ route('contacto') }} ">Contacto</a>
                            </li>

                            @auth
                                @if (auth()->user()->administrador == 1)
                                    <li class="nav-item">
                                        <a class="nav-link text-white" href=" {{ route('panel') }} ">Panel</a>
                                    </li>
                                @endif
                            @endauth 
                                   
                        </ul>
                        
                        <form method="GET" action=" {{ route('buscarProducto')}}" class="form-inline my-2 my-lg-0 ">
                            @csrf
                            <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar producto" aria-label="Search">
                            <button class="btn btn-success my-2 my-sm-0" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>  
                    </div>
                    

                    @guest

                        <div class="ml-3 border border-light">
                            <a href="" class="ml-3 estiloLink1"  data-toggle="modal" data-target="#exampleModalCenter"> Ingresar </a>
                            <a href="{{Route('register')}}" class="ml-3 estiloLink1"  > Registrarme </a>
                            <button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">
                            <i class="fas fa-user-circle fa-2x text-light"></i>
                            </button>
                        </div>
                        
                    @else

                        <div class="ml-3 border border-light">
                            <div class="btn-group dropdown">
                                <button type="button" class="btn text-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                Hola {{auth()->user()->name}}
                                @if (auth()->user()->id_avatar != 'NULL')
                                        <a href="{{route('perfil')}}"><img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 50px" src="{{Storage::url(auth()->user()->id_avatar)}}"></a>
                                    @else           
                                        <a href="{{route('perfil')}}"><img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 50px" src="{{asset('img/user.png')}}"></a>
                                        
                                    @endif 
                                </button>

                                <div class="dropdown-menu">

                                    <a href="{{route('perfil')}}" class="dropdown-item">Mi perfil</a>

                                    <a href="{{route('comprasUsuario')}}" class="dropdown-item">Compras</a>



                                    <a  class="dropdown-item" href="{{ route('logout') }}"  
                                    onclick="event.preventDefault();
                                    document.getElementById('logout-form').submit();">
                                    {{ __('Cerrar sesión') }}
                                    </a>   
                                    
                                

                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    @csrf
                                </form>
                                </div>
                            </div>
                        </div>
                    @endguest
        
                   
                    
                </div>
        
            </nav>
        </header>


        @yield('content')



        <!-- Modal -->
        <div class="modal fade text-center" id="exampleModalCenter" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="col-12 user-img">
                <img src="{{asset('img/user.png')}}" th:src="@{/img/user.png}" />
                </div>            

                <form method="POST" action="{{ route('login') }}">
                    @csrf

                    <div class="form-group row">
                        <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail') }}</label>

                        <div class="col-md-6">
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row">
                        <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Contraseña') }}</label>

                        <div class="col-md-6">
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>
                    </div>

                    <div class="form-group row ">
                        <div class="col-md-6 offset-md-4">
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                                <label class="form-check-label" for="remember">
                                    {{ __('Recordarme') }}
                                </label>
                            </div>
                        </div>
                    </div>
                    
                    <div class="row d-flex justify-content-center form-group row mb-0">
                       
                       
                            <div class="col-lg-12">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Ingresar') }}
                                </button>
                            </div>
                           

                           <div class="col">
                            @if (Route::has('password.request'))
                                
                            <a class="btn btn-link text-dark" href="{{ route('password.request') }}">
                                {{ __('Olvidaste tu contraseña?') }}
                            </a>
                        @endif

                        @if (Route::has('register'))
                            
                            <a class="btn btn-link text-dark" href="{{ route('register') }}">
                                {{ __('Registrarse') }}
                            </a>
                        @endif

                            </div>
                            

                           
                            
                        
                    </div>
            </div>
        </div>
    </div>
    
    

    <!-- Ini. Footer / Sticky -->
    <footer>
        <div class="sticky-container">
            <ul class="sticky">

                @auth
                    <li>
                        <img src="{{asset('img/carrito.png')}}" width="32" height="32">
                        <p><a href="{{route('carrito')}}" >Carrito de<br>Compras</a></p>
                    </li>
                @endauth

                
            </ul>
        </div>
    </footer>
    <!-- Fin Footer -->
    
    </body>

</html>