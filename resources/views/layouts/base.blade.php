<!DOCTYPE html>

    <html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">

        <!-- Los iconos tipo Solid de Fontawesome-->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.8/css/solid.css">
                
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
        <link rel="stylesheet" href="{{asset('./css/style.css')}}">
        <link rel="stylesheet" type="text/css" href="{{asset('./css/index.css')}}" th:href="@{/css/index.css}">
        {{-- <link rel="stylesheet" type="text/css" href="css/style.css"> --}}
        
        <!-- CSRF Token -->
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <!-- Scripts -->
        <script src="{{ asset('js/app.js') }}" defer></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous" defer>  </script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous" defer>  </script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous" defer >  </script>
        <script src="https://use.fontawesome.com/releases/v5.0.7/js/all.js" defer></script>
            
        <title>Integralísimo</title>

    </head>

    <header>    

        {{-- XS --}}
        <nav>
            <form method="GET" action=" {{ route('buscarProducto')}}" class="pt-2 pb-2 d-block d-sm-none color-marron mb-3">
                @csrf
                <div class="container-fluid">
                    <div class="row justify-content-around">

                        <div class="col-4">
                            <a class="navbar-brand mx-auto d-block mt-1" href=" {{ route('home') }} "><img src="{{asset('./img/logo/logoNuevoBlanco.png')}}" alt="" class="img-fluid"></a>
                        </div>

                        <div class="col-6 mt-2">                            
                            <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar producto" aria-label="Search">
                        </div>

                        <div class="col-2">
                            <button class="btn btn-success my-2 my-sm-0" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </div>

                        <div class="col">

                        </div>
                                                
                    </div>
                </div>            
            </form>
        </nav>
        {{-- XS --}}
        

        <nav class="navbar navbar-expand-lg navbar-light color-marron mb-4 pr-4 d-none d-sm-block">

            <div class="container-fluid d-flex justify-content-around">
                <button class="navbar-toggler " type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon  "></span>
                </button>
                <div class="col d-flex">
                    <a class="navbar-brand mx-auto d-block" href=" {{ route('home') }} "><img src="{{asset('./img/logo/logoNuevoBlanco.png')}}" alt="" class="img-fluid"></a>
                </div>
                
                <!-- Button trigger modal -->    
    
                <div class="collapse navbar-collapse ml-5" id="navbarSupportedContent">
                    <ul class="navbar-nav mr-auto ">
                        <li class="nav-item active">
                            <a class="nav-link text-white" href="{{ route('home') }}">Home <span class="sr-only">(current)</span></a>
                        </li>                
                        

                        <li class="nav-item">
                        <a class="nav-link text-white " href=" {{ route('productos') }}">Productos</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-white" href=" {{ route('faq') }} ">F.A.Q</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link text-white" href=" {{ route('contacto') }} ">Contacto</a>
                        </li>

                        @auth
                            @if (auth()->user()->administrador == 1)
                                <li class="nav-item">
                                    <a class="nav-link text-white" href=" {{ route('compras') }} ">Panel</a>
                                </li>
                            @endif
                        @endauth 

                        <form method="GET" action=" {{ route('buscarProducto') }}" class="form-inline mx-lg-5">
                            @csrf
                            <input name="busqueda" class="form-control mr-sm-2" type="search" placeholder="Buscar producto" aria-label="Search">
                            <button class="btn btn-success my-2 my-sm-0" type="submit">
                                <i class="fas fa-search"></i>
                            </button>
                        </form>
                               
                    </ul>
                    
                      

                </div>
                

                @guest

                    <div>
                        <a href="{{Route('register')}}">Ingresar</a>
                        <a href="{{Route('register')}}">Registrarme</a>
                        <button type="button" class="btn" data-toggle="modal" data-target="#exampleModalCenter">
                        <i class="fas fa-user-circle fa-2x text-light"></i>
                        </button>
                    </div>
                    
                @else
                
                    <a class="text-light"  href="{{route('carrito')}}">
                        <i class="fas fa-shopping-cart fa-2x"></i>                           
                    </a>  

                    <div class="ml-3">
                        
                        <div class="btn-group dropdown">

                       
                            <button type="button" class="btn text-white dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            Hola {{auth()->user()->name}}
                            @if (auth()->user()->id_avatar != 'NULL')
                                    <a href="{{route('perfil')}}"><img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 50px" src="{{Storage::url(auth()->user()->id_avatar)}}"></a>
                                @else           
                                    <a href="{{route('perfil')}}"><img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 50px" src="{{asset('img/user.png')}}"></a>                                        
                                @endif 
                            </button>

                            <div class="dropdown-menu">

                                <a href="{{route('perfil')}}" class="dropdown-item">Mi perfil</a>
                                <a href="{{route('comprasUsuario')}}" class="dropdown-item">Compras</a>

                                <a  class="dropdown-item" href="{{ route('logout') }}"  
                                onclick="event.preventDefault();
                                document.getElementById('logout-form').submit();">
                                {{ __('Cerrar sesión') }}
                                </a>                                                               

                            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                @csrf
                            </form>
                            </div>
                        </div>
                    </div>
                @endguest
                                   
            </div>    
        </nav>

    </header>
    


    <body class="fondoRosaClaro pb-5">               

        <div class="container-fluid pb-5">

            @yield('content')

        </div>            
  
    </body>

    <!-- Ini. Footer  -->
    <footer>
        
        <div class="row text-center text-light fixed-bottom justify-content-around color-marron d-sm-none pt-1 color-marron">
           

            <div class="col-3 dropdown">
                
                <a class="btn text-light" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    <i class="fas fa-bars fa-lg mt-2"></i>
                    <p>Menú</p>
                </a>

                <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">    
                    <a href="{{route('contacto')}}" class="dropdown-item">Contacto</a>                  
                    <a href="{{route('faq')}}" class="dropdown-item">FAQ</a>                   
                    <a href="{{route('home')}}" class="dropdown-item">Inicio</a>   
                </div>
                
            </div>
            
 

            <div class="col-3 mt-3">
                <a class="text-light" href="{{ route('productos') }}">
                    <i class="fas fa-columns fa-lg"></i>
                    <p>Productos</p>  
                </a>                                     
            </div>

            <div class="col-3 mt-3">
                <a class="text-light" href="{{route('carrito')}}">
                    <i class="fas fa-shopping-cart fa-lg"></i>
                    <p>Carrito</p>
                </a>                                        
            </div>

            <div class="col-3 dropdown">
                
                <a class="btn text-light" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    @guest
                        <i class="fas fa-user-circle fa-lg mt-2"></i>
                        <p>Cuenta</p>    
                    @endguest

                    @auth
                        <img class="rounded mx-auto rounded-circle img-fluid " style="max-height: 33px" src="{{asset('img/user.png')}}">
                        <p> {{auth()->user()->name}}</p>                                       
                    @endauth
                    
                </a>

                @auth                    
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a href="{{route('perfil')}}" class="dropdown-item">Mi perfil</a>

                        @if (auth()->user()->administrador == 1)
                            <a class="dropdown-item" href=" {{ route('compras') }} ">Panel</a>                                        
                        @endif    

                        <a href="{{route('comprasUsuario')}}" class="dropdown-item">Compras</a>   

                        
                        <a  class="dropdown-item" href="{{ route('logout') }}"  
                        onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                        {{ __('Cerrar sesión') }} </a>  
                                                                                                                                
                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                            @csrf
                        </form>
                    </div>                        
                @endauth

                @guest
                    <div class="dropdown-menu" aria-labelledby="dropdownMenuLink">
                        <a href="{{route('register')}}" class="dropdown-item">Registrarse</a>   
                        <a href="{{route('loginform')}}" class="dropdown-item">Iniciar sesión</a>  
                    </div>
                @endguest

              </div>

            

        </div>            
    </div>
            
</footer>

</html>