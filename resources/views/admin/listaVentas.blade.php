@extends('admin.paneladministrativo')


@section('content')
    

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">

                <form method="POST" action="{{ route('buscarventa') }}">
                    @csrf
                    <div class="input-group mb-3">
                      <input type="text" class="form-control" name="busqueda" placeholder="Ingresar búsqueda" aria-label="Recipient's username" aria-describedby="basic-addon2">
                      <div class="input-group-append">
                        <button class="btn btn-success" type="submit"><i class="fas fa-search"></i></button>
                      </div>
                    </div>
                </form>


                <div class="card">
                    <div class="card-header text-center ">{{ __('Ventas') }}   </div>
                    <ul class="list-group">
                        <div class="card-body">      
                            
                            

                            <table class="table table-striped">
                                <thead>
                                  <tr>
                                    <th scope="col">Nº venta</th>
                                    <th scope="col">Hora y fecha</th>
                                    <th scope="col">Cliente</th>
                                    
                                    <th scope="col">Total</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @forelse ($compras as $compra)
                                        
                                        
                                        <tr>
                                            <th scope="row"><a href="{{route('venta', $compra)}}">{{ $compra->id }}</a>  </th>
                                            <td>{{$compra->created_at}}  </td>
                                            <td><a href="{{route('perfilcliente',$compra->comprador)}}">{{$compra->comprador->name}} {{$compra->comprador->apellido }}</a> </td>
                                            <td>$ {{ $compra->total() }}</td>
                                        </tr>
                                        

                                    @empty
                                        No existen ventas registradas
                                    @endforelse
                                </tbody>
                            </table>

                                
                        
                        </div>
                    </ul>
                    
                </div>
            </div>
        </div>
    </div>

@endsection