@extends('admin.paneladministrativo')

@section('content')
            
<div class="container">

    <div class="row d-flex justify-content-center">
        
        <div class="col">

            <h3 class="">Venta #{{$venta->id}} - {{$venta->created_at}}</h3>    

                <br>                
                
                @if (session('status'))
                        <div class="alert alert-success text-center">
                            {{ session('status') }}
                        </div>
                @elseif(session('no'))
                        <div class="alert alert-danger text-center">
                            {{ session('no') }}
                        </div>
                @endif
                
                <div class="row justify-content-center ">

                                <div class="col-11 col-sm-12 bg-light pt-4 border pb-4 justify-content-around ">

                                    <b>Resumen compra</b>    
                                    <div></div>                                
                                    <br>
                                    <b>Comprador:</b> {{$venta->comprador->name}} {{$venta->comprador->apellido}}
                                    <br>
                                    <b>Subtotal: </b>      ${{$venta->total()}}
                                    <br>
                                    <b>Envío:</b>           ${{$venta->total()}}

                                    <hr>
                                    <b>Total venta:</b>   ${{$venta->total()}}
                                    
                                </div>

                                <div class="col-11 mt-3 col-sm-12 bg-light border mb-3 pt-4 pb-4">
                                    <b>Dirección de envío</b> 
                                    <br>

                                    {{$venta->direccionDeEnvio->direccion}}
                                    {{$venta->direccionDeEnvio->altura}}
                                    <br>
                                    {{$venta->direccionDeEnvio->piso}}, 
                                    {{$venta->direccionDeEnvio->departamento}}
                                    <br>
                                    {{$venta->direccionDeEnvio->ciudad}}, 
                                    {{$venta->direccionDeEnvio->provincia}}, 
                                    {{$venta->direccionDeEnvio->cp}}

                                </div>
                               
                                @foreach ($venta->items as $item)
                                
                                <div class="col-11 col-sm-12 bg-light pt-4 border pb-4">

                                   <b>{{$item->producto->nombre}}</b> 
                                    

                                    <br>

                                    <div class="row pt-2 ">

                                        @foreach ($item->producto->imagenes as $imagen)
                                            <div class="col-5"><img style="" class="img-fluid" src="{{asset("/storage/$imagen->nombre")}}" alt=""> </div>                                                        
                                        @endforeach 
    
                                            <div class="col-7">
                                                {{$item->producto->descripcion}}
                                                <br>
                                                {{$item->producto->precio}} x {{$item->producto->cantidad}} unidad
                                            </div>
                                    </div>                                                                                                       
                                    
                                </div>                                                                                   
                             
                                @endforeach

                                
                    
                </div>                                
                
                <br>            
                                 
        </div>

    </div>
       
</div>

@endsection