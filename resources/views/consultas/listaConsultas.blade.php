@extends('admin.paneladministrativo')


@section('content')
    


<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header text-center ">{{ __('Consultas') }}   </div>
                <ul class="list-group">
                    <div class="card-body">                                        

                        <div class="row d-flex">
                            <div class="tituloArticulo col-3 border">Nombre  </div>
                            <div class="tituloArticulo col-3 border">Fecha</div>
                            <div class="tituloArticulo col-3 border">Email</div>
                            <div class="tituloArticulo col-3 border">Motivo</div> 
                        </div>
                        @forelse ($consultas as $consulta)
                            <div class="row d-flex">

                                <div class="col-3 border">{{$consulta->nombre}}  </div>
                                <div class="col-3 border">{{$consulta->created_at}}</div>
                                <div class="col-3 border"> {{$consulta->email}}</div>
                                <div class="col-3 border">{{ $consulta->motivo }}</div>                                                                                                                 
                            
                            </div>                                                     
                        @empty
                            <div class="row d-flex justify-content-center justify-content-center mt-3">
                                <br>
                                <h3 class=""> No se encuentran registros.</h3>
                            </div> 
                        @endforelse
                        



                       
                    
                    </div>
                </ul>
                
            </div>
        </div>
    </div>
</div>


@endsection