
@extends('layouts.base')


@section('content')
    <div class="container">

        <div class="row">

            <div class="form-group col-md-12">
                <h3 class="text-center">Contacto</h3>
            </div>
            <div class="col-sm-12 col-lg-6  mb-3">

                @if (session('status'))
                    <div class="alert alert-success text-center">
                        {{ session('status') }}
                    </div>
                @endif

                <!-- form  -->
                <form method="POST" action="{{ route('guardarConsulta') }}">
                    @csrf

                    <div class="form-row">

                    </div>
                    <p><span class="font-weight-bold">Dirección:</span> Rosario (Santa Fe)</p>

                    <p><span class="font-weight-bold">E-mail:</span> info@integralisimo.com.ar</p>

                    <p><span class="font-weight-bold">Teléfono:</span> 3413434343</p>

                    <p><span class="font-weight-bold">Fax:</span> 341445515</p>

                    <p><span class="font-weight-bold">Información:</span> Horario de atención: lunes a viernes de 9 a 17hs.</p>

 

                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="firstname">{{ __('Nombre') }}</label>
                            <input id="nombre" placeholder="Nombre" type="text" class="form-control @error('nombre') is-invalid @enderror" name="nombre" value="{{ old('nombre') }}" required autocomplete="nombre" autofocus>

                            @error('nombre')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>                        
                    </div>


                    <div class="form-row">
                        <div class="form-group col-md-12">
                            <label for="email">{{ __('Email') }}</label>
                            <input id="email" placeholder="@" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>                        
                    </div>


                    <div class="form-row">                    
                        <div class="form-group col-md-12">
                            <label for="motivo">Motivo</label>
                            <textarea class="form-control" id="motivo" name="motivo" rows="3"></textarea>
                        </div>
                    </div>    
                        <button type="submit" class="btn btn-success btn-lg">Enviar</button>

                </form>
            </div>

            <div class="col-sm-12 col-lg-6 embed-responsive embed-responsive-21by9">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3348.457799065403!2d-60.64219277359466!3d-32.938920732127514!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x95b7aba38227104f%3A0x9fdf83d844878fe6!2sFundaci%C3%B3n%20Libertad!5e0!3m2!1ses-419!2sar!4v1574225402953!5m2!1ses-419!2sar" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
            </div>

        </div>

        

    </div>

    </div>



@endsection
    