@extends('layouts.base')




@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8 shadow ">
            
                    
                    <form method="POST" action="{{ route('guardardomicilio') }}" enctype="multipart/form-data" class="mt-3 mb-3">
                        @csrf

                        <div class="form-group row">
                            <label for="direccion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Direccion') }}</label>

                            <div class="col-md-6">
                                <input id="direccion" type="text" class="form-control @error('direccion') is-invalid @enderror" name="direccion" value="{{ old('direccion') }}" required autocomplete="direccion" autofocus>

                                @error('direccion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                

                        <div class="form-group row">
                            <label for="altura" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Altura') }}</label>

                            <div class="col-md-6">
                                <input id="altura" type="text" class="form-control @error('altura') is-invalid @enderror" name="altura" value="{{ old('altura') }}" required autocomplete="altura">

                                @error('altura')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="piso" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Piso') }}</label>

                            <div class="col-md-6">
                                <input id="piso" type="text" class="form-control @error('piso') is-invalid @enderror" name="piso"  autocomplete="piso">

                                @error('piso')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="departamento" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Departamento') }}</label>

                            <div class="col-md-6">
                                <input id="departamento" type="text" class="form-control @error('departamento') is-invalid @enderror" name="departamento"  autocomplete="departamento">

                                @error('departamento')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ciudad" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Ciudad') }}</label>

                            <div class="col-md-6">
                                <input id="ciudad" type="text" class="form-control @error('ciudad') is-invalid @enderror" name="ciudad" required autocomplete="ciudad">

                                @error('ciudad')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="provincia" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Provincia') }}</label>

                            <div class="col-md-6">
                                <input id="provincia" type="text" class="form-control @error('provincia') is-invalid @enderror" name="provincia" required autocomplete="provincia">

                                @error('provincia')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="cp" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('CP') }}</label>

                            <div class="col-md-6">
                                <input id="cp" type="text" class="form-control @error('cp') is-invalid @enderror" name="cp"  autocomplete="cp">

                                @error('cp')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="aclaracion" class="tituloArticulo col-md-4 col-form-label text-md-right">{{ __('Comentario') }}</label>

                            <div class="col-md-6">
                                <input id="aclaracion" type="text" class="form-control @error('aclaracion') is-invalid @enderror" name="aclaracion"  autocomplete="aclaracion">

                                @error('aclaracion')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                        </div>



                        

                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                <button type="submit" class="btn btn-success">
                                    {{ __('Guardar') }}
                                </button>
                            </div>
                        </div>
                    </form>
      
    </div>
</div>
@endsection
