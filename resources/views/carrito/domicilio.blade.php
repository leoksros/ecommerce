@extends('layouts.base')

@section('content')
    
@php

    $boton = "";
    if(Cart::count() == 0)
    {
            $boton = "disabled";
    }

@endphp



<div class="container">
                
    

    

    <div class="row">

            

            <div class="col col-md-7">

                <div class="card w-100 mb-4">
                    
                    <div class="card-body">    
                        <h4>Detalles de envío: </h4>
                        <br>                    
                            
                            <div class="row  rounded">
                                    <div class="col col-sm-4 col-md-12">
                                            <p>{{auth()->user()->name}}  {{auth()->user()->apellido}}, {{auth()->user()->codigoarea}}-{{auth()->user()->telefono}} </p>    
                                            <p>{{auth()->user()->direccion}}  {{auth()->user()->altura}}</p> 
                                            <p>{{auth()->user()->provincia}}, {{auth()->user()->id_ciudad}}, {{auth()->user()->cp}} </p>        
                                            <a href="{{route('modificarPerfil')}}"><p>Editar lugar de entrega</p></a>                                                                  
                                    </div>
                                   
                            </div>
                    
    
                    </div>
                </div>

                <div class="card w-100 mb-4">
                    <div class="card-body">  
                            <h4>Método de pago: </h4>
                            <br>                      
                            
                            <div class="row  rounded">
                                    <div class="col col-sm-4 col-md-12">

                                            Forma de pago seleccionada                                                              
                                    </div>
                                   
                            </div>
                    
    
                    </div>
                </div>

            
                <div class="card w-100 mb-4">
                    <div class="card-body">
                         <h4>Detalle del pedido: </h4>
                    @forelse (Cart::content() as $item)
                            <div class="w-100 ">
                                
                                    <div class="card-body">
                                            <h5 class="card-title ">
                                                    <a class="tituloArticulo" href="{{url('producto')."/$item->id"}}">{{$item->name}}</a>
                                            </h5>
                                            
                                            <div class="row">
                                                    <div class="col col-sm-4">

                                                            

                                                           {{--  @foreach ($producto->imagenes as $imagen)
                                                             <img src="{{asset("/storage/$imagen->nombre")}}"  alt="" class="img-fluid align-middle ">
                                                            @endforeach --}}
                                                         {{-- <img src="{{asset("/storage/")}}" alt="" class="img-fluid tamanioArticulo"> --}}
                                                           
                                                            @foreach ($item->options as $valor => $campo)
                                                                    @foreach ($campo as $foto)
                                                                           
                                                                                    <img src="{{asset("/storage/$foto->nombre")}}" alt="" class="img-fluid tamanioArticulo">
                                                                           
                                                                    @endforeach
                                                                
                                                            @endforeach
                                                         
                                                    </div>
                                                    <div class="col col-sm-8">
                                                        <p class="card-text"> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, libero!</p>
                                                        <p> <b>Cantidad:</b>  {{$item->qty}}
                                                                {{-- <a href=""><img src="./img/articulosVenta/botonMenos.png" alt="" class="img-fluid botonMasMenos"></a>
                                                        <input type="text" name="num" class="col-sm-2" value="">
                                                                <a href="" ><img src="./img/articulosVenta/botonMas.png" alt="" class="img-fluid botonMasMenos"></a> --}}
                                                        </p>
                                                        
                                                        <p> <b>Precio:</b> {{$item->price}}</p>
                                                        <p> <b>Suma:</b> @php
                                                                       echo( $item->price*$item->qty);
                                                                        @endphp
                                                        </p>
                                                        <div class="d-flex justify-content-between">
                                                                <span class="font-weight-bold "></span> 
                                                                <a href="{{route('removerItem',$item->rowId)}}"><i class="fas fa-trash-alt primary-text-color "></i></a>   
                                                        </div>                                       
                                                </div>
                                            </div>
                                    
                    
                                    </div>
                            </div>
                    @empty
                    <div class="row d-flex justify-content-center  justify-content-center">

                            <h1 class="tituloArticulo">Tu carrito está vacío :)</h1>
                          </div>
                    @endforelse
                </div>
                </div>
            
            </div>
    
            <div class="col-sm-12 col-md-4 color-marron-letra ">
                    <div class="card" style="width: 18rem;">
                            <div class="card-body  fondoRosa">
                            <h4 class="card-title">Pedido</h5>

                                    <div class="row d-flex">
                                            <div class="col-sm-6 col-md-6 col-lg-6">
                                                            
                                                            <span class=" font-weight-bold">Subtotal: </span>
                                                            <br><br>
                                                            <span class=" font-weight-bold">Envío:</span>           
                                                            <br> <br>             
                                                            <span class=" font-weight-bold">Total:</span>
                                            </div>
                                            <div class="col-sm-6 col-md-6 col-lg-6 ">
                                                            <span class="">$       {{Cart::subtotal()}}</span>
                                                            <br><br>
                                                            <span class="">$ XXX</span>           
                                                            <br> <br>             
                                                            <span class="">$ {{Cart::subtotal()}}</span>
                                            </div>

                                    </div>
                                    <br>
                                   
                                    
                            <a href="{{route('registrarPedido')}}" class="btn btn-success btn-lg btn-block {{$boton}}" >Confirmar compra</a>
                            </div>
                    </div>

                    <div class="card mt-3" style="width: 18rem;">
                                    <div class="card-body ">
                                    <h4 class="card-title">Medios de pago</h5>

                                    <img src="./img/mercadoPago2.png" alt="" class="img-fluid">
                                    </div>
                            </div>


            </div>

    </div>


</div>


@endsection