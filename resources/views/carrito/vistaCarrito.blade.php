@extends('layouts.base')


@section('content')

        @php        

            $boton = "";
            if(Cart::count() == 0)
            {
                    $boton = "disabled";
            }

        @endphp

        <div class="container">
                
                <h3>Tu pedido: </h3>
                <br>

                @if (session('status'))
                        <div class="alert alert-success text-center">
                        {{ session('status') }}
                        </div>
                        @endif

                <div class="row">
                        
                        <div class="col col-md-7">

                                @forelse ($pedido as $item)
                                        <div class="card w-100 mb-4">
                                                <div class="card-body">
                                                        <h5 class="card-title ">
                                                                <a class="tituloArticulo" href="{{url('producto')."/$item->id"}}">{{$item->name}}</a>
                                                        </h5>
                                                        
                                                        <div class="row">
                                                                <div class="col col-sm-4">

                                
                                                                       
                                                                        @foreach ($item->options as $valor => $campo)
                                                                                @foreach ($campo as $foto)
                                                                                       
                                                                                                <img src="{{asset("/storage/$foto->nombre")}}" alt="" class="img-fluid tamanioArticulo">
                                                                                       
                                                                                @endforeach
                                                                            
                                                                        @endforeach
                                                                     
                                                                </div>
                                                                <div class="col col-sm-8">
                                                                        <p class="card-text"> Lorem, ipsum dolor sit amet consectetur adipisicing elit. Iure, libero!</p>
                                                                        <p> <b>Cantidad:</b>  {{$item->qty}}
                                                                           
                                                                        </p>
                                                                        
                                                                        <p> <b>Precio:</b> {{$item->price}}</p>
                                                                        <p> <b>Suma:</b> @php
                                                                                       echo( $item->price*$item->qty);
                                                                                        @endphp
                                                                        </p>
                                                                        <div class="d-flex justify-content-between">
                                                                                <span class="font-weight-bold "></span> 
                                                                                <a href="{{route('removerItem',$item->rowId)}}"><i class="fas fa-trash-alt primary-text-color "></i></a>   
                                                                        </div>                                       
                                                                </div>
                                                        </div>
                                                
                                
                                                </div>
                                        </div>
                                @empty
                                <div class="row d-flex justify-content-center  justify-content-center">

                                        <h1 class="tituloArticulo">Tu carrito está vacío :)</h1>
                                      </div>
                                @endforelse

                                
                                
                        </div>
                
                        <div class="col-sm-12 col-md-4 color-marron-letra ">

                                <div class="card" >
                                        <div class="card-body  fondoRosa">
                                        <h4 class="card-title">Pedido</h5>
            
                                                <div class="row d-flex">
                                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6">
                                                                        
                                                                        <span class=" font-weight-bold">Subtotal: </span>
                                                                        <br><br>
                                                                        <span class=" font-weight-bold">Envío:</span>           
                                                                        <br> <br>             
                                                                        <span class=" font-weight-bold">Total:</span>
                                                        </div>
                                                        <div class="col-6 col-sm-6 col-md-6 col-lg-6 ">
                                                                        <span class="">$       {{Cart::subtotal()}}</span>
                                                                        <br><br>
                                                                        <span class="">$ XXX</span>           
                                                                        <br> <br>             
                                                                        <span class="">$ {{Cart::subtotal()}}</span>
                                                        </div>
            
                                                </div>
                                                <br>
                                               
                                                                                
                                                <a href="{{route('domiciliocompra')}}" class="btn btn-success btn-lg btn-block {{$boton}}" >Comprar</a>
            
                                        </div>
            
                                </div>

                                {{-- <div class="card" >
                                        <div class="card-body  fondoRosa">
                                        <h4 class="card-title">Pedido</h5>

                                                <div class="row d-flex">
                                                        <div class="col-sm-6 col-md-6 col-lg-6">
                                                                        
                                                                        <span class=" font-weight-bold">Subtotal: </span>
                                                                        <br><br>
                                                                        <span class=" font-weight-bold">Envío:</span>           
                                                                        <br> <br>             
                                                                        <span class=" font-weight-bold">Total:</span>
                                                        </div>
                                                        <div class="col-sm-6 col-md-6 col-lg-6 ">
                                                                        <span class="">$       {{Cart::subtotal()}}</span>
                                                                        <br><br>
                                                                        <span class="">Sin costo</span>           
                                                                        <br> <br>             
                                                                        <span class="">$ {{Cart::subtotal()}}</span>
                                                        </div>

                                                </div>
                                                <br>
                                               
                                                
                                        <a href="{{route('domiciliocompra')}}" class="btn btn-success btn-lg btn-block {{$boton}}" >Comprar</a>
                                        </div>
                                </div> --}}

                                


                        </div>

                </div>


        </div>


@endsection

        
