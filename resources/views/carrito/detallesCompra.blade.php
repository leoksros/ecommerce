@extends('layouts.base')

@section('content')
    
@php

    $boton = "";
    if(Cart::count() == 0)
    {
            $boton = "disabled";
    }



    
@endphp



<div class="container">
                
    
        @if (session('status'))
                <div class="alert alert-success text-center">
                    {{ session('status') }}
                </div>
        @elseif(session('no'))
                <div class="alert alert-danger text-center">
                    {{ session('no') }}
                </div>
        @endif
    

    <div class="row">

            

            <div class="col-12">

                <div class="card w-100 mb-4">
                    
                    <div class="card-body">    
                        <h4>Detalles de envío: </h4>
                        <br>                    
                            
                            <div class="row  rounded">
                                    <div class="col-12">
                                            <p>{{auth()->user()->name}}  {{auth()->user()->apellido}}, {{auth()->user()->codigoarea}}-{{auth()->user()->telefono}} </p>  
                                            <p><a href="{{route('modificarPerfil')}}"></p>
                                                <p>Editar datos personales</p></a>
                                                <b>Dirección de entrega</b> - <a href="{{route('creardomicilio')}}"> Nuevo domicilio</a>

                                            
                                            @if ($domicilio == 0)
                                                <br>
                                                No tiene ninguna dirección asignada
                                                <br>
                                                <a href="{{route('creardomicilio')}}"> Agregar domicilio</a>
                                            @else

                                                <form method="POST" action="{{ route('controlpedido') }}" enctype="multipart/form-data" id="formulario">
                                                        @csrf
                                                        @foreach (auth()->user()->domicilios as $domicilio)                                                                                                                                           
                                                                <div class="border p-3 mt-3 mb-3">
                                                                        
                                                                        <p>{{$domicilio->direccion}}  {{$domicilio->altura}}</p> 
                                                                        <p>{{$domicilio->provincia}}, {{$domicilio->ciudad}}, {{auth()->user()->cp}} </p> 
                                                                        
                                                                        <div class="form-check">
                                                                                <input class="form-check-input" type="radio" name="idDomicilio" id="{{$domicilio->id}}" value="{{$domicilio->id}}">
                                                                                <label class="form-check-label" for="{{$domicilio->id}}"></label>
                                                                        </div> 
                                                                                 
                                                                        <p><a href="{{route('modificarPerfil')}}"></p>
                                                                        <p>Editar lugar de entrega</p></a>

                                                                        
                                                                        
                                                                </div>
                                                        @endforeach
                                                </form>

                                            @endif                                                                                                                                                      

                                                <br>
                                                
                                    </div>
                                    
                            </div>
                            
    
                    </div>
                    
                </div>
                <button class="btn btn-success btn-lg btn-block {{$boton}}" form="formulario">Continuar con la compra </button>

                
            
            </div>
    
           

    </div>


</div>


@endsection