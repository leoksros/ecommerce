<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Storage;
use Illuminate\Http\Request;
use App\Producto;
use App\Categoria;
use App\ImagenProducto;

class ProductoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categorias = Categoria::all();
        $productos = Producto::all();
        return view('productos.listaProductos',compact('productos','categorias'));
    }

    public function abm()
    {

        $productos = Producto::all();
        return view('productos.abm',compact('productos'));
    }

    public function busquedaAbm(Request $request)
    {
        
        $req = $request["busqueda"];
        $productos  = Producto::where('nombre',"LIKE","%$req%")->get();
        /* return view('resultadoBusqueda',compact('busqueda')); */
        return view('productos.abm',compact('productos'));
    }


    public function formularioProducto()
    {
        $categorias = Categoria::all();
        return view('productos.abmProducto',compact('categorias'));
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*Inicio de Validacion*/

           
            $reglas = [
                'nombre' => "string|min:3",
                'descripcion' => "string|min:3",
                'precio' => "numeric|min:0",
                'categoria_id' => "required",
                'stock' => "numeric|min:0",
                'productoimagen' => "image"
            ];
            $mensajes = [
                'string' => "El campo :attribute debe ser un texto",
                'min' => "El campo :attribute tiene un minimo de :min",
                'max' => "El campo :attribute tiene un maximo de :max",
                'numeric' => "El campo :attribute debe ser un numero",
                'integer' => "El campo :attribute debe ser un numero entero",
                'unique' => "El campo :attribute se encuntra repetido",
                'image' => "El campo :attribute debe ser una imagen"
            ];
            $this->validate($request, $reglas, $mensajes);
                    
        /*Fin de Validacion*/

        $producto = new Producto;
        $producto->nombre = $request["nombre"];
        $producto->precio = $request["precio"];
        $producto->stock = $request["stock"];
        $producto->categoria_id = $request["categoria_id"];
        $producto->descripcion = $request["descripcion"];        

        $producto->save();
        

        $productoimagen = new ImagenProducto;
        $ruta = $request->file("productoimagen")->store("public");
        $nombreImagen = basename($ruta);
        $productoimagen->nombre = $nombreImagen;
        $productoimagen->producto_id = $producto->id;

        
        $productoimagen->save();
        
        return redirect()->route('abmListaProductos')->with('status','El producto se registro exitosamente!');

    }

    public function show($id)

    {
      
        $producto = Producto::find($id);

        $categoriaProducto = $producto->categoria;
        $productosCategoria = $categoriaProducto->productos;      

        $imagenesProducto = ImagenProducto::where('producto_id','=',"$id")->get();        
        return view('productos.producto',compact('producto','imagenesProducto','productosCategoria'));

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function buscar(Request $request)
    {
        $categorias = Categoria::all();
        $req = $request["busqueda"];
        $productos  = Producto::where('nombre',"LIKE","%$req%")->get();
        /* return view('resultadoBusqueda',compact('busqueda')); */
        return view('productos.listaProductos',compact('productos','categorias'));
    }

    public function buscarABM(Request $request)
    {
        $req = $request["busqueda"];
        $productos  = Producto::where('nombre',"LIKE","%$req%")->get();
        /* return view('resultadoBusqueda',compact('busqueda')); */
        return view('listaProductos',compact('productos'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
   

    public function modificar(Producto $producto)
    {
        /* $producto = Producto::find($producto); */
        $categorias = Categoria::all();
        return view('productos.modificarProducto',compact('producto','categorias'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Producto $producto)
    {   

        $reglas = [
            'nombre' => "string|min:3",
            'descripcion' => "string|min:3",
            'precio' => "numeric|min:0",
            'categoria_id' => "required",
            'stock' => "numeric|min:0",
            'productoimagen' => "image"
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];
        $this->validate($request, $reglas, $mensajes);
        
        
        if(request()->file('productoimagen')){

            //Momenteano hasta que hagamos carrusel

            if($producto->imagenes){
                foreach($producto->imagenes as $imagen)
                {
                    Storage::delete("public/".$imagen->nombre);
                }               
            }
            //---

            $productoimagen = new ImagenProducto;
            $ruta = $request->file("productoimagen")->store("public");
            $nombreImagen = basename($ruta);
            $productoimagen->nombre = $nombreImagen;
            $productoimagen->producto_id = $producto->id;
            $productoimagen->save();

             
        }

        $producto->update([

            'nombre' => $request['nombre'],
            'precio' => $request['precio'],
            'stock' => $request['stock'],
            'categoria_id' => $request['categoria_id'],
            'descripcion' => $request['descripcion']

        ]);



        return redirect()->route('abmListaProductos')->with('status','El producto se actualizó exitosamente!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Producto $producto)
    {


        if(count($producto->item) == 0)
        {
            $producto->delete();
        
        return redirect()->route('abmListaProductos')->with('status','La producto ha sido eliminado');
        } 
        else
        {
            return redirect()->route('abmListaProductos')->with('no','El producto no puede ser eliminado. Posee relación con otros elementos.');
        }

    
    }
}
