<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Faq;





class FaqController extends Controller
{


    public function abm()
    {
        $faqs = Faq::all();
        return view('faqs.abmListaFAQ',compact('faqs'));
    }

    public function lista()
    {
        $faqs = Faq::all();
        return view('faqs.lista',compact('faqs'));
    }


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $preguntas = Faq::all();
        return view('faqs.faq',compact('preguntas'));
    }
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */




    public function store(Request $request)
    {

        $reglas = [
            'pregunta' => "string|min:3",
            'respuesta' => "string|min:3",

        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"                        
        ]; 


        $this->validate($request, $reglas, $mensajes);

        $faq = new Faq;

        $faq->pregunta = $request->pregunta;
        $faq->respuesta = $request->respuesta;

        $faq->save();

        return redirect()->route('listaFAQ')->with('status','Pregunta y respuesta registrada correctamente!');




    }

    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Faq $faq)
    {

        return view('faqs.modificarFAQ',compact('faq'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Faq $faq)
    {

        $reglas = [
            'pregunta' => "string|min:3",
            'respuesta' => "string|min:3",

        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"                        
        ]; 


        $this->validate($request, $reglas, $mensajes);

        
        $faq->update([
            
            'pregunta' => $request['pregunta'],
            'respuesta' => $request['respuesta']
        ]);

        return redirect()->route('abmListaFAQ')->with('status','FAQ modificada exitosamente !');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Faq $faq)
    {
    
        $faq->delete();

        return redirect()->route('listaFAQ')->with('status','FAQ eliminada.');

    }
}
