<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Item;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    /* public function __construct()
    {
        $this->middleware('auth');
    } */

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {


        $items = Item::all();
        $hola = [];

        if(count($items) != 0)
       { 
        $productosVendidos = $items->unique('id_producto'); 
       
       
        

        foreach($productosVendidos->values()->all() as $producto)
        {
            
            $suma = 0;

            foreach($items as $item)
            {                
                            
                if($item->id_producto == $producto->producto->id)
                {
                    
                    $suma = $suma + $item->cantidad;
                }               
            }

            $masVendido[$producto->producto->id] = $suma; 
            
           
        }

        $numbers = array(4, 6, 2, 22, 11);
       
        arsort($masVendido);
        

        foreach($masVendido as $idProducto => $cantidadVendida)
        {
            $productos[] = Producto::find($idProducto);            

        }
        
        $productos = collect($productos)->take(6);
        
        
       }
       else

       {

        $productos = collect($hola);  

       }
        
        
               
                            
        return view('home',compact('productos'));
    }
}
