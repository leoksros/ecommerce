<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Contacto;

class ContactoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('consultas.contacto');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        /*Inicio de Validacion*/

            $reglas = [
                'nombre' => "string|min:3",
                'motivo' => "string|min:3",
                'email' => 'email:rfc,dns'
            ];
            $mensajes = [
                'string' => "El campo :attribute debe ser un texto",
                'min' => "El campo :attribute tiene un minimo de :min",
                'max' => "El campo :attribute tiene un maximo de :max",
                'numeric' => "El campo :attribute debe ser un numero",
                'integer' => "El campo :attribute debe ser un numero entero",
                'unique' => "El campo :attribute se encuntra repetido",
                'email' => "El campo :attribute debe ser un E-mail"

            ];
            $this->validate($request, $reglas, $mensajes);
            
        /*Fin de Validacion*/

        /* dd($request); */
        $consulta = new Contacto;

        $consulta->nombre = $request['nombre'];
        $consulta->email = $request['email'];
        $consulta->motivo = $request['motivo'];

        $consulta->save();
        
        return redirect('contacto')->with('status','Consulta enviada. Nos contactaremos a la brevedad!');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $consultas = Contacto::all();
        return view('consultas.listaConsultas',compact('consultas'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
