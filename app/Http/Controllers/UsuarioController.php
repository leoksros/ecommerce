<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Ciudad;
use App\Provincia;
Use App\User;

class UsuarioController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function login()
    {
        return view('auth.login');
    }

    public function perfilcliente(User $cliente)
    {
        return view('admin.perfilcliente',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        return view('usuarios.perfil');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
     
        return view('usuarios.modificar');

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(User $user)
    {   
    
       if(request()->file('photo')){
            $path = request()->photo->store('public');
            if($user->id_avatar){
                Storage::delete('public/'.$user->id_avatar);
            }
        }
        
        $user->update([

            'name' => request()->name,
            'email' => request()->email,            
            'apellido' => request()->apellido,           
            'codigoarea' => request()->codArea,
            'telefono' => request()->telefono,          
            'id_avatar' => (isset($path)) ? basename($path): $user->id_avatar,

        ]);
        
        return redirect()->route('perfil')->with('status','Perfil actualizado.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function compras()
    {
        
        return view('compras.compras');
    }

    public function paneladministrador()
    {
        return view('admin.paneladministrativo');
    }
}
