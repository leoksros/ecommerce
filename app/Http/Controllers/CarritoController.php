<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Producto;
use App\Compra;
use App\Item;
use App\Domicilio;


use Cart;


class CarritoController extends Controller
{


    public function remover($item)
    {

        Cart::remove($item);
        return  redirect()->route('carrito')->with('status','Pedido actualizado');

    }


    public function show()
    {
        $pedido = Cart::content();
                
        return view('carrito.vistaCarrito',compact('pedido'));
    }


    public function agregar(Request $request, Producto $producto)
    {
        /*Inicio de Validacion*/

            $reglas = [
                'name' => "string|min:3",
                'price' => "numeric|min:0",
                'weight' => "numeric|min:0",
                'qty' => "numeric|min:1",
                'options' => [
                    'foto' => "image"
                ]
            ];
            $mensajes = [
                'string' => "El campo :attribute debe ser un texto",
                'min' => "El campo :attribute tiene un minimo de :min",
                'max' => "El campo :attribute tiene un maximo de :max",
                'numeric' => "El campo :attribute debe ser un numero",
                'integer' => "El campo :attribute debe ser un numero entero",
                'unique' => "El campo :attribute se encuntra repetido",
                'image' => "El campo :attribute debe ser una imagen"
            ];

            $this->validate($request, $reglas, $mensajes);
        
        /*Fin de Validacion*/   
       
        $productoPedido = Cart::add([
            'id' => $producto->id,
            'name' => $producto->nombre,
            'price' => $producto->precio,
            'weight' => 0,
            'qty' => $request->cantidad,
            'options' => [
                'foto' => $producto->imagenes
            ] 
        ]);


        

        return redirect('carrito');

    }


    public function seleccionardomicilio()
    {
        $domicilio = count(auth()->user()->domicilios);      
        return view('carrito.detallesCompra',compact('domicilio'));
    }


    public function registrarPedido(Domicilio $domicilio)
    {
        
        $compra = new Compra;
        $compra->id_user = Auth()->user()->id; 
        $compra->id_domicilio = $domicilio->id;
        $compra->total = 0; // asigno 0 para no borrar el atributo de la tabla
        $compra->save();
        
        foreach(Cart::content() as $producto )
        {
            $item = new Item;
            $item->id_compra = $compra->id;
            $item->id_producto = $producto->id;
            $item->cantidad = $producto->qty;
            $item->precio = $producto->price;
            
            $item->save();
        }

        Cart::destroy(); 

        return redirect()->route('perfil')->with('status','Compra registrada. Muchas gracias! ');                
        
    }

    public function controlpedido(Request $request)
    {

        if($request->idDomicilio == null)
        {
            return redirect()->route('domiciliocompra')->with('no','Para continuar debe agregar o seleccionar un domicilio de entrega.');
        }
        $domicilio = Domicilio::find($request->idDomicilio);
        
        return view('carrito.confirmacompra',compact('domicilio'));

    }
    
}
