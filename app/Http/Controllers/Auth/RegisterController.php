<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    
    protected function validator(array $data)
    {
        
        return Validator::make($data, [
            
            'name' => [/* 'required' ,*/ 'string', 'min:3', 'max:255'],
            'apellido' => [/* 'required' ,*/'string','min:3'],
            'direccion' => [/* 'required' ,*/ 'string', 'min:3'],
            'altura' =>  [/* 'required' ,*/ 'numeric', 'min:0'],
            'email' => ['required', 'string', 'email:rfc,dns', 'max:255', 'unique:users'],
            'codigoarea' => [/* 'required', */ 'numeric', 'min:0'],
            'telefono' => [/* 'required', */ 'numeric', 'min:0'],
            'cp' => [/* 'required', */ 'numeric', 'min:0'],
            'avatar' => ['image'],
            'password' => ['required', 'min:8', 'confirmed']                    
            
        ]);
        

        /*Inicio de Validacion*/
/*
        $reglas = [
            'name' => "string|min:3",
            'apellido' => "string|min:3",
            'direccion' => "string|min:3",
            'altura' => "numeric|min:0",
            'email' => "email:rfc,dns",
            'codigoarea' => "numeric|min:0",
            'telefono' => "numeric|min:0",
            'cp' => "numeric|min:0",
            'avatar' => "image"
        ];
        $mensajes = [
            'string' => "El campo :attribute debe ser un texto",
            'min' => "El campo :attribute tiene un minimo de :min",
            'max' => "El campo :attribute tiene un maximo de :max",
            'numeric' => "El campo :attribute debe ser un numero",
            'integer' => "El campo :attribute debe ser un numero entero",
            'unique' => "El campo :attribute se encuntra repetido",
            'image' => "El campo :attribute debe ser una imagen"
        ];
        $this->Validator($data, $reglas, $mensajes);
            
        /*Fin de Validacion*/



    }
    

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
     
        


        if(request()->id_avatar)
        {
            $path = request()->id_avatar->store('public');
        }
        else
        {
            $path = "NULL";
        }

        return User::create([
            
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            /* 'apellido' => $data['apellido'], */
            /* 'direccion' => $data['direccion'], */
            /* 'altura' => $data['altura'], */
            /* 'codigoarea' => $data['codigoarea'], */
            /* 'telefono' => $data['telefono'], */
            /* 'id_ciudad' => $data['id_ciudad'], */
            /* 'provincia' => $data['provincia'], */
            /* 'cp' => $data['cp'], */
            'id_avatar' => basename($path),
            'administrador' => 0

        ]);

    }

    

   
}
