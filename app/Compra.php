<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Compra extends Model
{
    public $guarded = [];
    public $timestamps = true;
    
    public function items()
    {
        return $this->hasMany("App\Item","id_compra");
    }

    public function total()
    {
            
        $total = 0;

        foreach($this->items as $lineaDeCompra)
        {
            $total = $total + $lineaDeCompra->cantidad*$lineaDeCompra->precio;
            
        }
        
        return $total;
    }

    public function direccionDeEnvio()
    {

        return $this->belongsTo("App\Domicilio","id_domicilio");

    }

    public function comprador()
    {
        return $this->belongsTo("App\User","id_user");
    }
    



}
