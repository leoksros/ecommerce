<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Item extends Model
{
    public $guarded = [];
    public $timestamps = true;

    public function producto()
    {
        return $this->belongsTo("App\Producto","id_producto");
    }

    public function sumar()
    {
        
    }

}
