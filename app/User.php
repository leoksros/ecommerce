<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use Notifiable;

    public function compras()
    {
        return $this->hasMany("App\Compra","id_user");
    }

    public function provinciaUsuario()
    {
       
            return $this->belongsTo("App\provincia","provincia");
       
    }

    public function domicilios()
    {
        return $this->hasMany("App\Domicilio","id_user");
    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','apellido','direccion','altura','codigoarea','telefono','id_ciudad','provincia','cp','id_avatar','administrador'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
}
