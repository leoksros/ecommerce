<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Provincia extends Model
{
    public $guarded = [];



    public function ciudad()
    {
       
            return $this->hasMany("App\Ciudad","id_provincia");
       
    }

}
