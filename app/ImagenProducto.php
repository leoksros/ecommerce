<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ImagenProducto extends Model
{
    public $table = "imagenproductos";
    public $guarded = [];

}
