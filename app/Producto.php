<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Producto extends Model
{
    public $guarded = [];

    public function imagenes()
    {
        return $this->hasMany("App\ImagenProducto","producto_id");
    }

    public function categoria()
    {
        return $this->belongsTo("App\Categoria","categoria_id");
    }

    public function item()
    {
        return $this->hasMany("App\Item",'id_producto');
    }

}
